FROM golang:alpine as builder
COPY hello /go/hello
RUN cd /go/hello && go build -ldflags="-s -w" hello.go

FROM scratch
WORKDIR /app
COPY --from=builder /go/hello/hello .
CMD [ "./hello"] 